# SSI Wallet

For testing, once can use whitelabel implementation of the Aries Mobile Agent React Native. It is a whitelabel implementation of Aries agent written in React Native, thus compatible with Android and iOS.

[Repository](https://github.com/hyperledger/aries-mobile-agent-react-native)

## Building

To enable the app, you need to have mediator already running. Check the logs of the mediator and copy the Invite URL.

* Install build tools Yarn and Node 16.15
  * `npm install -g yarn`
  * `nvm install 16.15 `
* Clone the repository
* enter the clonder repository directory
* run `yarn install`
* install legacy packages `cd packages/legacy && yarn run prepare`
* configure Mediator instance
  * `touch packages/legacy/app/.env`
  * put line with the copied invitation url into the file like `MEDIATOR_URL=<invitation_url>`

## Running

You can run the app in an emulator or on some device.

### Run on device (Android)

* Follow the guideline to setup Ract Native - [guide](https://reactnative.dev/docs/environment-setup)

* Connect your device to the development machine
* Navigate to the root directory of the cloned repository
* Start Metro - `cd packages/legacy/app && yarn start`
* In a new terminal window start the app `cd packages/legacy/app && yarn run android`


