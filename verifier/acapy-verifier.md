# aries-cloud-agent-python for Verifier role

ACApy (Aries Cloud Agent Python) is an implementation of the Hyperledger Aries project able to communicate with the Hyperledger Indy ledger.

[ACApy repository](https://github.com/hyperledger/aries-cloudagent-python)

The agent exposes either HTTP endpoint or WebSocket (WS) endpoint. The agent is accompanied by a database used by the agent to store its data.

## Requirements

* machine with IP / domain name
* installation of Docker engine on the machine
* web server to act as reverse-proxy and forward requests to docker containers
* Docker-compose to run the container

## Running

* Build Docker container of the aca-py
* Create docker-compose
* Spin up the instance with `docker-compose up -d`
* Expose interfaces into the world (e.g. using Apache webserver reverse-proxy)

### Building Docker container - ACA-py

* Clone acapy repository
* enter clonned directory
* build the image with Docker `docker build -f docker/Dockerfile -t aries-acapy .`
  * to allow `indy` wallet type, build the _indy_ version of Dockerfile: `docker build -f docker/Dockerfile.indy -t aries-acapy .`

### Docker-compose file

```
---
version: '3'

networks:
  dev-verifier-network:

services:
  agent:
    image: aries-cloudagent-run:latest
    container_name: dev-verifier-acapy
    depends_on:
      - db
    networks:
      - dev-verifier-network
    ports:
      - "${VERIFIER_AGENT_HTTP_ADMIN_PORT_HOST}:${VERIFIER_AGENT_HTTP_ADMIN_PORT}"
      - "${VERIFIER_AGENT_HTTP_IN_PORT_HOST}:${VERIFIER_AGENT_HTTP_IN_PORT}"
      - "${VERIFIER_AGENT_WS_IN_PORT_HOST}:${VERIFIER_AGENT_WS_IN_PORT}"
    environment:
      WAIT_HOSTS: "dev-verifier-db:${VERIFIER_AGENT_DB_PORT}"
      WAIT_HOSTS_TIMEOUT: "300"
      WAIT_SLEEP_INTERVAL: "5"
      WAIT_HOST_CONNECT_TIMEOUT: "3"
    entrypoint: /bin/bash
    command: [
       "-c",
       #        "curl -k -L -H 'Bypass-Tunnel-Reminder: yes' ${VERIFIER_CONTROLLER_WEBHOOK_URL}/bypass; \
        "curl -d '{\"seed\":\"${VERIFIER_AGENT_WALLET_SEED}\", \"role\":\"TRUST_ANCHOR\", \"alias\":\"${VERIFIER_AGENT_LABEL}\"}' -X POST ${VERIFIER_AGENT_LEDGER_URL}/register; \
        sleep 5; \
        aca-py start \
        --label '${VERIFIER_AGENT_LABEL}' \
        --auto-provision \
        --auto-ping-connection \
        --auto-accept-requests \
        --auto-accept-invites \
        --auto-respond-credential-request \
        --public-invites \
        --preserve-exchange-records \
        --emit-new-didcomm-prefix \
        --inbound-transport 'http' '0.0.0.0' '${VERIFIER_AGENT_HTTP_IN_PORT}' \
        --inbound-transport 'ws' '0.0.0.0' '${VERIFIER_AGENT_WS_IN_PORT}' \
        --outbound-transport 'http' \
        --outbound-transport 'ws' \
        --ws-timeout-interval 30 \
        --ws-timeout-interval 300 \
        --enable-undelivered-queue \
        --admin '0.0.0.0' '${VERIFIER_AGENT_HTTP_ADMIN_PORT}' \
        --admin-insecure-mode \
        --genesis-url '${VERIFIER_AGENT_LEDGER_URL}/genesis' \
        --seed '${VERIFIER_AGENT_WALLET_SEED}' \
        --endpoint '${VERIFIER_AGENT_HTTP_ENDPOINT_URL}' '${VERIFIER_AGENT_WS_ENDPOINT_URL}' \
        --wallet-type '${VERIFIER_AGENT_WALLET_TYPE}' \
        --wallet-name '${VERIFIER_AGENT_WALLET_NAME}' \
        --wallet-key '${VERIFIER_AGENT_WALLET_KEY}' \
        --wallet-storage-type '${VERIFIER_AGENT_WALLET_STORAGE_TYPE}' \
        --wallet-storage-config '{\"url\":\"dev-verifier-db:5432\",\"max_connections\":5}'
        --wallet-storage-creds '{\"account\":\"${VERIFIER_DATABASE_USERNAME}\",\"password\":\"${VERIFIER_DATABASE_PASSWORD}\",\"admin_account\":\"${VERIFIER_DATABASE_USERNAME}\",\"admin_password\":\"${VERIFIER_DATABASE_PASSWORD}\"}'
        --notify-revocation \
        --trace \
        --trace-target log \
        --trace-tag acapy.uni.verifier \
        --trace-label acapy.uni.verifier \
        --log-level debug"
        # \
        #        --webhook-url ${VERIFIER_CONTROLLER_WEBHOOK_URL}#${VERIFIER_CONTROLLER_WEBHOOK_API_KEY}"
    ]
  db:
    image: postgres:latest
    container_name: dev-verifier-db
    hostname: dev-verifier-db
    networks:
      - dev-verifier-network
    environment:
      POSTGRES_USER: ${VERIFIER_DATABASE_USERNAME}
      POSTGRES_PASSWORD: ${VERIFIER_DATABASE_PASSWORD}
      POSTGRES_DB: ${VERIFIER_DATABASE_DB}
    volumes:
      - ./.postgres:/var/lib/postgresql
```

## Configuration

The docker-compose file expects some variables to be either hardcoded or passed via the .env file. For the example docker-compose file above, we have used .env file like:

```
VERIFIER_DATABASE_USERNAME=postgres
VERIFIER_DATABASE_PASSWORD=[PASS]
VERIFIER_DATABASE_DB=dev_verifier

VERIFIER_AGENT_HTTP_IN_PORT=9380
VERIFIER_AGENT_HTTP_IN_PORT_HOST=9380
VERIFIER_AGENT_WS_IN_PORT=9382
VERIFIER_AGENT_WS_IN_PORT_HOST=9382
VERIFIER_AGENT_HTTP_ADMIN_PORT=9384
VERIFIER_AGENT_HTTP_ADMIN_PORT_HOST=9384
VERIFIER_AGENT_DB_PORT=5342
VERIFIER_AGENT_WEBHOOK_URL=
VERIFIER_AGENT_LABEL=Faber University
VERIFIER_AGENT_LEDGER_URL=https://verifier.server.net
VERIFIER_AGENT_HTTP_ENDPOINT_URL=https://verifier.server.net:9381
VERIFIER_AGENT_WS_ENDPOINT_URL=https://verifier.server.net:9383
VERIFIER_AGENT_WALLET_SEED=[SEED]
VERIFIER_AGENT_WALLET_TYPE=askar
VERIFIER_AGENT_WALLET_NAME=dev_verifier_wallet
VERIFIER_AGENT_WALLET_KEY=[WALLET_KEY]
VERIFIER_AGENT_WALLET_STORAGE_TYPE=postgres_storage
```

### Example reverse proxy in Apache

The stack exposes several endpoints:

* HTTP endpoint for agent API
* WS endpoint for agent API
* Swagger UI to control agent (admin ui)
* HTTP endpoint for Tails server

We have been using Apache webserver to expose these endpoints with SSL enabled and forward the traffic into corresponding container.


Example configuration of VirtualHost:

```
# VERIFIER - HTTP
<VirtualHost *:9381>
        ServerName verifier.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/verifier-http-error.log
        CustomLog ${APACHE_LOG_DIR}/verifier-http-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / http://0.0.0.0:9380/
        ProxyPassReverse / http://0.0.0.0:9380/
</VirtualHost>

# VERIFIER - WS
<VirtualHost *:9383>
        ServerName verifier.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/verifier-ws-error.log
        CustomLog ${APACHE_LOG_DIR}/verifier-ws-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / ws://0.0.0.0:9382/
        ProxyPassReverse / ws://0.0.0.0:9382/
</VirtualHost>

# VERIFIER - ADMIN
<VirtualHost *:9385>
        ServerName verifier.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/verifier-admin-error.log
        CustomLog ${APACHE_LOG_DIR}/verifier-admin-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / http://0.0.0.0:9384/
        ProxyPassReverse / http://0.0.0.0:9384/
</VirtualHost>

```

