# aries-cloud-agent-python for Issuer role

ACApy (Aries Cloud Agent Python) is an implementation of the Hyperledger Aries project able to communicate with the Hyperledger Indy ledger.

[ACApy repository](https://github.com/hyperledger/aries-cloudagent-python)
[Tails server repository](https://github.com/bcgov/indy-tails-server)

The agent exposes either HTTP endpoint or WebSocket (WS) endpoint. The agent is accompanied by a _Tails_ server, which records revocation of credentials. Last component of the stack is a database used by the agent to store its data.

## Requirements

* machine with IP / domain name
* installation of Docker engine on the machine
* web server to act as reverse-proxy and forward requests to docker containers
* Docker-compose to run the container

## Running

* Build Docker container of the aca-py
* Build Docker container of the Tails server
* Create docker-compose
* Spin up the instance with `docker-compose up -d`
* Expose interfaces into the world (e.g. using Apache webserver reverse-proxy)

### Building Docker container - ACA-py

* Clone acapy repository
* enter clonned directory
* build the image with Docker `docker build -f docker/Dockerfile -t aries-acapy .`
  * to allow `indy` wallet type, build the _indy_ version of Dockerfile: `docker build -f docker/Dockerfile.indy -t aries-acapy .`

### Building Docker container - TAILS

* Clone Tails repository
* enter clonned directory
* build the image with Docker `docker build -f docker/Dockerfile.tails-server .`

### Docker-compose file

```
---
version: '3'

networks:
  dev-issuer-network:

services:
  agent:
    image: aries-cloudagent-run:latest
    container_name: dev-issuer-acapy
    depends_on:
      - db
    networks:
      - dev-issuer-network
    ports:
      - "${ISSUER_AGENT_HTTP_ADMIN_PORT_HOST}:${ISSUER_AGENT_HTTP_ADMIN_PORT}"
      - "${ISSUER_AGENT_HTTP_IN_PORT_HOST}:${ISSUER_AGENT_HTTP_IN_PORT}"
      - "${ISSUER_AGENT_WS_IN_PORT_HOST}:${ISSUER_AGENT_WS_IN_PORT}"
    environment:
      WAIT_HOSTS: "dev-issuer-db:${ISSUER_AGENT_DB_PORT}"
      WAIT_HOSTS_TIMEOUT: "300"
      WAIT_SLEEP_INTERVAL: "5"
      WAIT_HOST_CONNECT_TIMEOUT: "3"
    entrypoint: /bin/bash
    command: [
       "-c",
       #        "curl -k -L -H 'Bypass-Tunnel-Reminder: yes' ${ISSUER_CONTROLLER_WEBHOOK_URL}/bypass; \
        "curl -d '{\"seed\":\"${ISSUER_AGENT_WALLET_SEED}\", \"role\":\"TRUST_ANCHOR\", \"alias\":\"${ISSUER_AGENT_LABEL}\"}' -X POST ${ISSUER_AGENT_LEDGER_URL}/register; \
        sleep 5; \
        aca-py start \
        --label '${ISSUER_AGENT_LABEL}' \
        --auto-provision \
        --auto-ping-connection \
        --auto-accept-requests \
        --auto-accept-invites \
        --auto-respond-credential-request \
        --public-invites \
        --preserve-exchange-records \
        --emit-new-didcomm-prefix \
        --inbound-transport 'http' '0.0.0.0' '${ISSUER_AGENT_HTTP_IN_PORT}' \
        --inbound-transport 'ws' '0.0.0.0' '${ISSUER_AGENT_WS_IN_PORT}' \
        --outbound-transport 'http' \
        --outbound-transport 'ws' \
        --ws-timeout-interval 30 \
        --ws-timeout-interval 300 \
        --enable-undelivered-queue \
        --admin '0.0.0.0' '${ISSUER_AGENT_HTTP_ADMIN_PORT}' \
        --admin-insecure-mode \
        --genesis-url '${ISSUER_AGENT_LEDGER_URL}/genesis' \
        --seed '${ISSUER_AGENT_WALLET_SEED}' \
        --endpoint '${ISSUER_AGENT_HTTP_ENDPOINT_URL}' '${ISSUER_AGENT_WS_ENDPOINT_URL}' \
        --wallet-type '${ISSUER_AGENT_WALLET_TYPE}' \
        --wallet-name '${ISSUER_AGENT_WALLET_NAME}' \
        --wallet-key '${ISSUER_AGENT_WALLET_KEY}' \
        --wallet-storage-type '${ISSUER_AGENT_WALLET_STORAGE_TYPE}' \
        --wallet-storage-config '{\"url\":\"dev-issuer-db:5432\",\"max_connections\":5}'
        --wallet-storage-creds '{\"account\":\"${ISSUER_DATABASE_USERNAME}\",\"password\":\"${ISSUER_DATABASE_PASSWORD}\",\"admin_account\":\"${ISSUER_DATABASE_USERNAME}\",\"admin_password\":\"${ISSUER_DATABASE_PASSWORD}\"}'
        --tails-server-base-url '${ISSUER_TAILS_SERVER_URL}' \
        --notify-revocation \
        --trace \
        --trace-target log \
        --trace-tag acapy.uni.issuer \
        --trace-label acapy.uni.issuer \
        --log-level debug"
        # \
        #        --webhook-url ${ISSUER_CONTROLLER_WEBHOOK_URL}#${ISSUER_CONTROLLER_WEBHOOK_API_KEY}"
    ]
  db:
    image: postgres:latest
    container_name: dev-issuer-db
    hostname: dev-issuer-db
    networks:
      - dev-issuer-network
    environment:
      POSTGRES_USER: ${ISSUER_DATABASE_USERNAME}
      POSTGRES_PASSWORD: ${ISSUER_DATABASE_PASSWORD}
      POSTGRES_DB: ${ISSUER_DATABASE_DB}
    volumes:
      - ./.postgres:/var/lib/postgresql
  tails-server:
    image: tails-server:latest
    container_name: dev-issuer-tails
    networks:
      - dev-issuer-network
    ports:
      - "${ISSUER_TAILS_PORT_HOST}:${ISSUER_TAILS_PORT}"
    environment:
      GENESIS_URL: "${ISSUER_AGENT_LEDGER_URL}/genesis"
      TAILS_SERVER_URL: "${ISSUER_TAILS_SERVER_URL}"
    command: >
      tails-server
        --host 0.0.0.0
        --port ${ISSUER_TAILS_PORT}
        --storage-path /tmp/dev-tails-files
        --log-level INFO
```

## Configuration

The docker-compose file expects some variables to be either hardcoded or passed via the .env file. For the example docker-compose file above, we have used .env file like:

```
ISSUER_DATABASE_USERNAME=postgres
ISSUER_DATABASE_PASSWORD=[PASS]
ISSUER_DATABASE_DB=dev_issuer

ISSUER_AGENT_HTTP_IN_PORT=9280
ISSUER_AGENT_HTTP_IN_PORT_HOST=9280
ISSUER_AGENT_WS_IN_PORT=9282
ISSUER_AGENT_WS_IN_PORT_HOST=9282
ISSUER_AGENT_HTTP_ADMIN_PORT=9284
ISSUER_AGENT_HTTP_ADMIN_PORT_HOST=9284
ISSUER_AGENT_DB_PORT=5342
ISSUER_AGENT_WEBHOOK_URL=
ISSUER_AGENT_LABEL=Faber University
ISSUER_AGENT_LEDGER_URL=https://issuer.server.net
ISSUER_AGENT_HTTP_ENDPOINT_URL=https://issuer.server.net:9281
ISSUER_AGENT_WS_ENDPOINT_URL=https://issuer.server.net:9283
ISSUER_AGENT_WALLET_SEED=[SEED]
ISSUER_AGENT_WALLET_TYPE=askar
ISSUER_AGENT_WALLET_NAME=dev_issuer_wallet
ISSUER_AGENT_WALLET_KEY=[WALLET_KEY]
ISSUER_AGENT_WALLET_STORAGE_TYPE=postgres_storage

ISSUER_TAILS_PORT=9286
ISSUER_TAILS_PORT_HOST=9286
ISSUER_TAILS_SERVER_URL=https://issuer.server.net:9287
```

### Example reverse proxy in Apache

The stack exposes several endpoints:

* HTTP endpoint for agent API
* WS endpoint for agent API
* Swagger UI to control agent (admin ui)
* HTTP endpoint for Tails server

We have been using Apache webserver to expose these endpoints with SSL enabled and forward the traffic into corresponding container.


Example configuration of VirtualHost:

```
# ISSUER - HTTP
<VirtualHost *:9281>
        ServerName issuer.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/issuer-http-error.log
        CustomLog ${APACHE_LOG_DIR}/issuer-http-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / http://0.0.0.0:9280/
        ProxyPassReverse / http://0.0.0.0:9280/
</VirtualHost>

# ISSUER - WS
<VirtualHost *:9283>
        ServerName issuer.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/issuer-ws-error.log
        CustomLog ${APACHE_LOG_DIR}/issuer-ws-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / ws://0.0.0.0:9282/
        ProxyPassReverse / ws://0.0.0.0:9282/
</VirtualHost>

# ISSUER - ADMIN
<VirtualHost *:9285>
        ServerName issuer.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/issuer-admin-error.log
        CustomLog ${APACHE_LOG_DIR}/issuer-admin-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / http://0.0.0.0:9284/
        ProxyPassReverse / http://0.0.0.0:9284/
</VirtualHost>

# ISSUER - TAILS
<VirtualHost *:9287>
        ServerName issuer.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/issuer-tails-error.log
        CustomLog ${APACHE_LOG_DIR}/issuer-tails-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / http://0.0.0.0:9286/
        ProxyPassReverse / http://0.0.0.0:9286/
</VirtualHost>
```

