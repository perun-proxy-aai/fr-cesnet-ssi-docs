# ACApy

ACApy (Aries Cloud Agenty Python) is an implementation of an Aries agent in Python.

[Repository](https://github.com/hyperledger/aries-cloudagent-python)

## Configuration

Configuration parameters are described in the alongisde .txt files.

## Provisioning parameters

```
ACA-Py invocations are separated into two types - initially provisioning an agent (provision) and starting a new agent process (start). This separation enables not having to pass in some encryption-related parameters required for provisioning when starting an agent instance. This improves security in production deployments.
```

